package br.com.caiosaderio.cotacao;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.caiosaderio.cotacao.task.ServiceTask;

public class MainActivity extends AppCompatActivity implements AsyncResponse {

    private final String urlCotacao = "http://developers.agenciaideias.com.br/cotacoes/json";
    private TextView indiceBovespa;
    private TextView variacaoBovespa;
    private TextView indiceDolar;
    private TextView variacaoDolar;
    private TextView indiceEuro;
    private TextView variacaoEuro;
    private TextView dataAtualizacao;
    private Button btnAtualizar;
    private ServiceTask task;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        indiceBovespa = (TextView) findViewById(R.id.indiceBovespa);
        variacaoBovespa = (TextView) findViewById(R.id.variacaoBovespa);
        indiceDolar = (TextView) findViewById(R.id.indiceDolar);
        variacaoDolar = (TextView) findViewById(R.id.variacaoDolar);
        variacaoEuro = (TextView) findViewById(R.id.variacaoEuro);
        indiceEuro = (TextView) findViewById(R.id.indiceEuro);
        dataAtualizacao = (TextView) findViewById(R.id.dataAtualizacao);
        btnAtualizar = (Button) findViewById(R.id.btnAtualizar);
        //PRIMEIRA EXECUCAO
        executeServiceTask();
        //EXECUTA NO CLIQUE
        btnAtualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeServiceTask();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(task != null){
            task.cancel(true);
        }
    }

    public void executeServiceTask(){
        task = new ServiceTask(this);
        task.execute(urlCotacao);
        task.delegate = this;
    }

    @Override
    public void processFinishing(String json) {
        if(json != null){
            try {
                JSONObject obj = new JSONObject(json);
                JSONObject bovespa = obj.getJSONObject("bovespa");
                indiceBovespa.setText(bovespa.getString("cotacao"));
                variacaoBovespa.setText(bovespa.getString("variacao"));
                JSONObject dolar = obj.getJSONObject("dolar");
                indiceDolar.setText(dolar.getString("cotacao"));
                variacaoDolar.setText(dolar.getString("variacao"));
                JSONObject euro = obj.getJSONObject("euro");
                indiceEuro.setText(euro.getString("cotacao"));
                variacaoEuro.setText(euro.getString("variacao"));
                dataAtualizacao.setText(obj.getString("atualizacao"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
