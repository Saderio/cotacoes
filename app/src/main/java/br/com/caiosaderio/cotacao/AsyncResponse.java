package br.com.caiosaderio.cotacao;

/**
 * Created by caiosaderio on 9/22/15.
 */
public interface AsyncResponse {
    void processFinishing(String output);
}
