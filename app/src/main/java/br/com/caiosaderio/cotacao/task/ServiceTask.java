package br.com.caiosaderio.cotacao.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;

import br.com.caiosaderio.cotacao.AsyncResponse;
import br.com.caiosaderio.cotacao.suporte.WebClient;

/**
 * Created by caiosaderio on 9/22/15.
 */
public class ServiceTask extends AsyncTask<String, Void, String> {

    private Context ctx;
    private ProgressDialog dialog;
    private WebClient webClient;
    public AsyncResponse delegate = null;

    public ServiceTask(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(ctx, "Download", "Obtendo Cotações");
    }

    @Override
    protected String doInBackground(String... params) {
        webClient = new WebClient(params[0]);
        try{
            return webClient.conectar();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String json) {
        super.onPostExecute(json);
        delegate.processFinishing(json);
        dialog.dismiss();
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
        dialog.dismiss();
    }
}
