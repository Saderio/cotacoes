package br.com.caiosaderio.cotacao.suporte;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by caiosaderio on 9/21/15.
 */
public class WebClient {
    private String url;

    public WebClient(String url) {
        this.url = url;
    }

    public String conectar() throws IOException{
        String retorno;
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.connect();
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();

            String linha = null;
            while ((linha = reader.readLine()) != null){
                sb.append(linha + "\n");
            }
            retorno = sb.toString();
        }finally {
            conn.disconnect();
        }
        return retorno;
    }

}
